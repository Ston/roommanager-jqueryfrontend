import { TableController } from "./js/roomoverview";
import { ReservationController } from "./js/roomReservation";

const tableCtrl:TableController = new TableController("overviewTable");
const reserveCtrl:ReservationController = new ReservationController();

$(function(){
  $("#overview").load("./resources/roomoverview.html", () => {
    tableCtrl.createTable()
  });
  $("#reserveRoom").load("./resources/reserve-room.html")

  init();
});

function init() {
  $("#reserveRoom").hide();
  $("#reserveRoom").on("click", "#switch", () => { toggleElements(); });
  reserveCtrl.init();
  $("#overview").on("click", ".reserveRoomBtn", () => { toggleElements(); });
}

function toggleElements() {
  $("#reserveRoom").toggle();
  $("#overview").toggle();

  if($("#overview").is(":visible")){
    tableCtrl.updateData();
    tableCtrl.createTable();
  }
  if($("#reserveRoom").is(":visible")){
    reserveCtrl.fillRoomInfo();
  }
}
