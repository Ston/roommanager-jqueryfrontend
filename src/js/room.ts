export class Room {
  name:string;
  isBooked: boolean;
  constructor(name:string, isBooked: boolean) {
    this.name = name;
    this.isBooked = isBooked;
  }
}
