import { Room } from './room';
import * as roomService from './roomService';

export class TableController {
  private rooms: Room[];
  private tableId: string;

  constructor(tableId:string) {
    this.updateData();
    this.tableId = tableId;
  }

  updateData(): void {
    roomService.getRooms().then(res => {
      this.rooms = res;
    });
  }

  createTable():void {
    console.log("table created");
    let tableBuildArray:string[] = [];
    for(let room of this.rooms) {
      tableBuildArray.push(this.createRow(room));
    }
    $("#" + this.tableId).html(tableBuildArray.join(''));

    $("#" + this.tableId).find(".reserveRoomBtn").each(function(){
      let roomName:string = $(this).parent().siblings("td:first").text();
      $(this).click(() => {
        roomService.getRoom(roomName).then(res => {
          roomService.setSelected(res);
        });
      });
    });
  }

  private createRow(room: Room) {
    return `
      <tr>
        <td>${room.name}</td>
        <td>${room.isBooked ? "belegt" : "frei"}</td>
        <td><button class="btn btn-default reserveRoomBtn">Reservieren</button></td>
      </tr>
    `;
  }
}
