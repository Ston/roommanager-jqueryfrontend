import { Room } from "./room";

const ROOMARRAY:Room[] = [
  new Room("London", true),
  new Room("Paris", false),
  new Room("Berlin", true),
]

var selectedRoom:Room = new Room("undefined", false);

export function getRooms(): Promise<Room[]> {
  return new Promise((resolve, reject) => {
    resolve(ROOMARRAY);
  });
}

export function addRoom(room:Room): void {
  ROOMARRAY.push(room);
}

export function getRoom(name:string): Promise<Room> {
  return new Promise((resolve, reject) => {
    resolve(ROOMARRAY.find((room:Room) => room.name === name));
  });
}

export function setSelected(room:Room): void {
  selectedRoom = room;
}

export function getSelected(): Room {
  return selectedRoom;
}
