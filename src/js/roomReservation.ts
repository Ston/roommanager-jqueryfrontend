import { Room } from './room';
import * as roomService from './roomService';

export class ReservationController {
  private room:Room = roomService.getSelected();

  init(): void {
    $("#reserveRoom").on("click", "#book", () => {
      this.toggleReservation();
      this.fillRoomInfo();
     });
  }

  fillRoomInfo(): void {
    this.room = roomService.getSelected();
    $("#roomName").text(this.room.name);
    $("#roomStatus").text(this.room.isBooked ? "belegt" : "frei");
    $("#book").text(this.room.isBooked ? "Reservierung aufheben" : "Reservieren");
  }

  toggleReservation(): void {
    this.room.isBooked = !this.room.isBooked;
  }
}
